//
//  SearchLocationViewController.swift
//  FindingNemo
//
//  Created by The Son    on 7/25/16.
//  Copyright © 2016 Savvycom. All rights reserved.
//

import UIKit
import GoogleMaps

// Define a constant as the tableViewCellIdentifier
let SearchLocationTableViewCellIdentifier:String = "SearchLocationTableViewCellIdentifier"

// Decleare a closure, which will be triggered after users chooses any locations
var completionBlock:((GMSAutocompletePrediction)->Void)?

class SearchLocationViewController: UIViewController {
    
    @IBOutlet var searchTextField:SearchLocationTextField!
    @IBOutlet var placeTableView:UITableView!
    @IBOutlet var emptyLabel:UILabel!
    
    var searchString:String?
    var places:Array<GMSAutocompletePrediction>!
    var placeClient:GMSPlacesClient!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initObjects()
        self.setupUI()
    }
    
    func initObjects() -> Void {
        
        placeClient = GMSPlacesClient()
        places = Array()

        placeTableView.backgroundColor = UIColor.clearColor()
        placeTableView.tableFooterView = UIView()
        
        // 2 lines below make the tableView cell be dynamic of height
        placeTableView.estimatedRowHeight = 50
        placeTableView.rowHeight = UITableViewAutomaticDimension
        
        // Tableview register nib for using the customized cell
        placeTableView.registerNib(UINib(nibName: "SearchLocationTableViewCell", bundle: nil), forCellReuseIdentifier: SearchLocationTableViewCellIdentifier)

        if !(searchString?.isEmpty)! {
            searchTextField.text = searchString
            self.autoCompletePlaceWithPlaceString(searchString!)
        }
    }
    
    func setupUI() -> Void {
        
        // Create the left view for search field.
        // Set the mode is Always for keeping it always display in the field
        let backButton = UIButton(type: .Custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 40, height: CGRectGetHeight(searchTextField.frame))
        backButton.setImage(UIImage(named: "gray-back-icon"), forState: .Normal)
        backButton.addTarget(self, action: #selector(SearchLocationViewController.goBack(_:)), forControlEvents: .TouchUpInside)
        
        searchTextField.leftView = backButton
        searchTextField.leftViewMode = .Always
        searchTextField.placeholder = "Search"
        searchTextField.clearButtonMode = .Always
        
        emptyLabel.text = "No results found"
    }
    
    override func viewDidAppear(animated: Bool) {
        searchTextField.becomeFirstResponder()
    }
    
    func goBack(sender:UIButton!) -> Void {
        // Hide keyboard
        self.view.endEditing(true)
        
        // Dismiss search screen then comeback to the map screen
        self .dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: <-- TableView DataSource -->
    // Return total numbers of row in the tableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return places.count
    }
    
    // This function will fill all places we have from an AutoComplete API call into the tableView
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(SearchLocationTableViewCellIdentifier, forIndexPath: indexPath) as! SearchLocationTableViewCell
        
        if let prediction:GMSAutocompletePrediction = places[indexPath.row] {
//            cell.locationNameLabel.attributedText = prediction.attributedFullText
            cell.locationPrimaryNameLabel.attributedText = prediction.attributedPrimaryText
            cell.locationSecondaryNameLabel.attributedText = prediction.attributedSecondaryText
        }
        
        return cell
    }
    
    // MARK: <-- TableView Delegate -->
    // When user chooses a place then go back and trigger the closure
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let prediction:GMSAutocompletePrediction = places[indexPath.row] {
            
            // Trigger the colosure
            completionBlock?(prediction)
            
            // Then dismiss the screen
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // MARK: <-- TextField Delegate -->
    // When user taps to clear button then clear all data and show empty label
    func textFieldShouldClear(textField: UITextField) -> Bool {
        emptyLabel.hidden = false
        places.removeAll()
        placeTableView.reloadData()
        return true
    }
    
    // Each time user texts, we will make an autocomplete API call then fill the data into the tableView
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let nextAppearedString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        self.autoCompletePlaceWithPlaceString(nextAppearedString)
        return true
    }
    
    var searchingKeyword:String?
    // MARK: <-- Search locations by using Autocomplete API -->
    func autoCompletePlaceWithPlaceString(keyword:String) -> Void {
        searchingKeyword = keyword
//        let filter = GMSAutocompleteFilter()
//        filter.type = GMSPlacesAutocompleteTypeFilter.City
        placeClient.autocompleteQuery(keyword, bounds: nil, filter: nil) { (predictions, error) in
            if (self.searchingKeyword != keyword) {
                return
            }
            if (predictions==nil) {
                self.emptyLabel.hidden = false
                self.places.removeAll()
            } else {
                self.emptyLabel.hidden = true
                self.places = predictions!
            }
            self.placeTableView.reloadData()
        }
    }
    
    // This function will be invoked when user chooses any place from the tableView
    func searchLocationWithResult(completion:(GMSAutocompletePrediction) -> Void) {
        completionBlock = completion
    }
}
